/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dannylowpass
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    

    @Test
    public void testGetCartSizeRegular() {
        System.out.println("getCartSize Reuglar");
        Cart instance = new Cart();
        Product sampleProduct = new Product("Orange", 1.00);
        instance.addProduct(sampleProduct);
        int expResult = 1;
        int result = instance.getCartSize();
        assertEquals(expResult, result);
        
    }
    
    @Test
    public void testGetCartSizeException() {
        System.out.println("getCartSize Execption");
        Cart instance = new Cart();
        Product sampleProduct = new Product("Orange", 1.00);
        instance.addProduct(sampleProduct);
        int expResult = 2;
        int result = instance.getCartSize();
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
}
