/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author dannylowpass
 */
public class DiscountByPercentage extends Discount {

    @Override
    public double calculateDiscount(double price) {
      return price * (1 - (0.01 * amount));  
    }
    
}
