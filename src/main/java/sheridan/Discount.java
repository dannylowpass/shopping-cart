/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author dannylowpass
 */
public abstract class Discount {
    
    double amount;
    
    public double getAmount(){
        return amount;
    }
    
    public abstract double calculateDiscount(double price);
}
