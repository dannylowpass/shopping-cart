/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author dannylowpass
 */
public class DiscountFactory {
   
    private static DiscountFactory DisFactory;
    
    private DiscountFactory(){};
    
    public static DiscountFactory getInstance(){
        
        if(DisFactory == null){
            DisFactory = new DiscountFactory();
        } return DisFactory;
    }
    
    public Discount getDiscount(DiscountType type, double price, double amount){
        
        Discount d = null;
        
        switch ( type ){
        
        case AMOUNT :
            d = new DiscountByAmount();
            break;
            
        case PERCENTAGE : 
            d = new DiscountByPercentage();
            break;
        } 
        return d;
    }
}

